<?php

namespace App\Http\Controllers;

use App\Models\Main;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('index');
    }

    public function validationAjax(Request $request)
    {
        if($request->ajax()){
            $post = $request->post();
            $column = $request['column'];
            $rules = [
                'co_email' => 'required|email|unique:data_vis',
                'per_firstname' => 'required',
                'per_lastname' => 'required',
                'co_name'=>'required',
                'co_mobile'=>'phone:AUTO,mobile',
                'co_jobtitle'=>'required',
                'per_designation'=>'required',
            ];
            $value=[$request['column']=>$request['value'],'_token'=>$request['_token']];
            $one[$column]=@$rules[$column];
            $validator = Validator::make($value, $one);
            $attributeNames = array(
                'co_email' => 'Email',
            );
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {
                return response()->json($validator->getMessageBag()->toArray(),422);
            }
            return response()->json(['column'=>$column],200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request,[
            'co_email' => 'required|email|unique:data_vis',
            'per_firstname' => 'required',
            'per_lastname' => 'required',
            'co_name'=>'required',
            'co_mobile'=>'phone:AUTO,mobile',
            'co_jobtitle'=>'required',
            'per_designation'=>'required',
        ]);

        $model = new Main();
        $model->co_email = $request->co_email;
        $model->per_firstname = $request->per_firstname;
        $model->per_lastname = $request->per_lastname;
        $model->co_name = $request->co_name;
        $model->co_mobile = $request->co_mobile;
        $model->co_jobtitle = $request->co_jobtitle;
        $model->per_designation = $request->per_designation;
        $model->co_country = 'MALAYSIA';

        if($model->save()){
            unset($model['id']);
            unset($model['created_at']);
            unset($model['updated_at']);
            Mail::send('rsvp2',['model'=>$model->toArray()], function($message) use ($model)  {
                $message->to($model->co_email)->subject('Virtual Carrer Expo - Registration Confirmation');
                $message->from(['rsvp@virtualcareerexpo.my'=>'Virtual Carrer Expo']);
            });
            $request->session()->flash('alert-success', 'Thank you. You will receive a confirmation email shortly.');
        } else {
            $request->session()->flash('alert-danger', 'Registration Failed');
        }

        return response()->redirectToRoute('onscreen',['id'=>$model->rsvp_logs]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function onscreen($id){
        $find = Main::where('rsvp_logs',$id)->firstOrFail();
        return response()->view('onscreen',['model'=>$find->toArray()]);
    }

}
