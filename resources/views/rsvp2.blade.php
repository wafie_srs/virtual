@extends('layouts.master')

@section('content')
    <table>
        <tr>
            <td colspan="2">
                <img src="https://virtualcareerexpo.1id.my/images/banner.png" width="100%">
            </td>
        </tr>
        <tr><td colspan="2"><p>Launching of Virtual Career Expo Business Events Edition 2020</p></td></tr>
        <tr><td colspan="2"><p>Thank you for registering!.</p></td></tr>
        <tr><td style="white-space: nowrap">Name</td><td width="100%">: {{ $model['per_designation'] }} {{ $model['per_firstname'] }} {{ $model['per_lastname'] }}</td></tr>
        <tr><td style="white-space: nowrap">Company</td><td width="100%">: {{ $model['co_name'] }}</td></tr>
        <tr><td style="white-space: nowrap">Date</td><td width="100%">: 9th Nov 2020</td></tr>
        <tr><td style="white-space: nowrap">Registration Starts</td><td>: 11.00am</td></tr>
        <tr><td style="white-space: nowrap">Event Starts</td><td>: 11.30am – 2:30pm</td></tr>
        <tr><td style="white-space: nowrap">Venue</td><td>: Ballroom, Courtyard by Marriott Penang</td></tr>
        <tr><td colspan="2">
                <p>Please arrive earlier to check in as the event practices strict SOP.</p>
                <p>
                    <b>HEALTH DECLARATION</b><br>
                    To prevent the spread of COVID-19 and reduce the risk of exposure, please click <a href="https://docs.google.com/forms/d/e/1FAIpQLSegN1wMJuIHzz8WD75g--SEMTfHtE2WjUsp9atRaSBISk_zEg/viewform?usp=sf_link">here</a> to declare your health condition.
                </p>
                <p>
                    <b>SOCIAL DISTANCING REMINDER</b><br>
                    Please remember to keep a distance of at least 1 metre between yourself and others. Wearing a mask is mandatory throughout the whole event. Your cooperation is an important precautionary measure to everyone!
                </p>
                <p>We look forward to your attendance. For any additional enquires, do feel free to drop us an email.</p>
                <p>Please submit any questions to: hello@virtualcareerexpo.my</p>
                <p>Thank you.</p>
                <p class="text-center"><img style="background-color:white;padding:10px;width:170px" src="http://app.1id.my/site/qr-generator2?params={{ $model['rsvp_logs'] }}"></p>
            </td></tr>
    </table>

@endsection
